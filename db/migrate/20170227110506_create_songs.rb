class CreateSongs < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.string :name
      t.string :name_en
      t.string :name_s
      t.string :code, null: false
      t.text :lyrics_vni
      t.text :lyrics
      t.integer :views_count, default: 0
      t.integer :plays_count, default: 0

      t.timestamps
    end

    add_index :songs, :name
    add_index :songs, :name_en
    add_index :songs, :name_s
    add_index :songs, :code
    add_index :songs, :views_count
    add_index :songs, :plays_count
  end
end
