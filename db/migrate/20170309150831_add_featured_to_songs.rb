class AddFeaturedToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :featured, :boolean, default: false

    add_index :songs, :featured
  end
end
