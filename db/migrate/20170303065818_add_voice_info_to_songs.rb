class AddVoiceInfoToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :man_supported, :boolean
    add_column :songs, :woman_supported, :boolean

    add_index :songs, :man_supported
    add_index :songs, :woman_supported
    add_index :songs, [:man_supported, :woman_supported]
  end
end
