# frozen_string_literal: true
module VniUnicodeConverter
  @csv_uni_chars = {}
  @csv_vni_chars = {}

  @uni_chars1 = %w(
    Ấ ấ
    Ầ ầ Ẩ ẩ Ẫ ẫ Ậ ậ Ắ ắ
    Ằ ằ Ẳ ẳ Ẵ ẵ Ặ ặ Ế ế Ề ề Ể ể
    Ễ ễ Ệ ệ Ố ố Ồ ồ Ổ ổ Ỗ ỗ
    Ộ ộ Ớ ớ Ờ ờ Ở ở Ỡ ỡ
    Ợ ợ Ố ố Ồ ồ Ổ ổ Ỗ ỗ
    Ộ ộ Ớ ớ Ờ ờ Ở ở Ỡ ỡ
    Ợ ợ Ứ ứ Ừ ừ
    Ử ử Ữ ữ Ự ự
  )

  @vni_chars1 = %w(
    AÁ aá
    AÀ aà AÅ aå AÃ aã AÄ aä AÉ aé
    AÈ aè AÚ aú AÜ aü AË aë EÁ eá EÀ eà EÅ eå
    EÃ eã EÄ eä OÁ oá OÀ oà OÅ oå OÃ oã
    OÄ oä ÔÙ ôù ÔØ ôø ÔÛ ôû ÔÕ ôõ
    ÔÏ ôï OÁ oá OÀ oà OÅ oå OÃ oã
    OÄ oä ÔÙ ôù ÔØ ôø ÔÛ ôû ÔÕ ôõ
    ÔÏ ôï ÖÙ öù ÖØ öø
    ÖÛ öû ÖÕ öõ ÖÏ öï
  )

  @uni_chars = %w(
    Ơ ơ ĩ Ị ị
    À Á Â Ã È É Ê Ì Í Ò
    Ó Ô Õ Ù Ú Ý à á â ã
    è é ê ì í ò ó ô õ ù
    ú ý Ă ă Đ đ Ĩ Ũ ũ
    Ư ư Ạ ạ Ả ả Ẹ ẹ
    Ẻ ẻ Ẽ ẽ Ỉ ỉ Ọ ọ
    Ỏ ỏ Ụ ụ Ủ ủ Ỳ ỳ Ỵ ỵ
    Ỷ ỷ Ỹ ỹ
  )

  @vni_chars = %w(
    Ô ô ó Ò ò
    AØ AÙ AÂ AÕ EØ EÙ EÂ Ì Í OØ
    OÙ OÂ OÕ UØ UÙ YÙ aø aù aâ aõ
    eø eù eâ ì í oø où oâ oõ uø
    uù yù AÊ aê Ñ ñ Ó UÕ uõ
    Ö ö AÏ aï AÛ aû EÏ eï
    EÛ eû EÕ eõ Æ æ OÏ oï
    OÛ oû UÏ uï UÛ uû YØ yø Î î
    YÛ yû YÕ yõ
  )

  def self.init_data
    return if @csv_uni_chars.present? && @csv_vni_chars.present?

    Rails.logger.info 'Loading chars...'
    File.readlines(Rails.root.join('lib', 'vni', 'vni_unicode.csv')).each do |line|
      chars = line.strip.split(',')
      @csv_uni_chars[chars[2]] = chars[0]
      @csv_vni_chars[chars[2]] = chars[3].split(' ').map { |n| n.downcase.hex }
    end
  end

  def self.convert(str)
    converted_bytes = chars_convert(str.unpack('C*'), @vni_chars1, @uni_chars1, 1)
    converted_bytes = chars_convert(converted_bytes, @vni_chars, @uni_chars, 2)
    converted_str = converted_bytes.pack('U*')
    converted_str
  end

  def self.chars_convert(unpacked_str, vni_chars, _uni_chars, _type, _times = 1)
    vni_chars.each_with_index do |chr, _chr_i|
      vni_chr_bytes = @csv_vni_chars[chr]
      vni_chr_bytes_length = vni_chr_bytes.length

      unpacked_str.each_with_index do |_str_chr_byte, str_chr_byte_i|
        unpacked_bytes = unpacked_str[str_chr_byte_i, vni_chr_bytes_length]
        uni_chr_bytes = @csv_uni_chars[chr].unpack('U*')

        next if unpacked_bytes != vni_chr_bytes || unpacked_bytes == uni_chr_bytes

        unpacked_str[str_chr_byte_i, vni_chr_bytes_length] = uni_chr_bytes

        # prefix = '=' * times * 2
        # uni_char = uni_chr_bytes.pack('U*')
        # packed_str = unpacked_str.pack('U*')
        # p "|#{prefix}=> MATCH! #{chr} replaced with: #{uni_char} - #{packed_str}"

        # chars_convert(unpacked_str, vni_chars, uni_chars, type, times + 1)
        # break
      end
    end

    unpacked_str
  end
end
