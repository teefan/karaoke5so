# frozen_string_literal: true
require 'rc4'
require 'midi/midi_sequence'

namespace :midi do
  desc 'Extract midi files'
  task extract: :environment do
    # Open a database
    db = SQLite3::Database.new(Rails.root.join('lib', 'midi', 'midi.db').to_s)

    # Open midi data
    data = File.open(Rails.root.join('lib', 'midi', 'midi.dat').to_s, 'rb')

    FileUtils.mkdir_p(Rails.root.join('lib', 'midi', 'extracted').to_s)

    db.execute('SELECT DISTINCT language FROM songtable') do |row|
      FileUtils.mkdir_p(Rails.root.join('lib', 'midi', 'extracted', row[0]).to_s)
    end

    # Find a few rows
    # WHERE CAST(id as INTEGER) > 49999
    db.execute('SELECT * FROM songtable') do |row|
      # Rails.logger.info row

      # Init RC4 decryptor
      decryptor = RC4.new(ENV['RC4_KEY'])

      id, loc, len, _var1, _var2, song_name, lang, _var3, song_name_search, _var5, _var6, _var7, _var8, _var9, _var10,
        _var11, song_name_en = row

      Rails.logger.info "Extracting song #{id} - #{song_name}..."

      data.pos = loc.to_i
      encrypted_song_data = data.read(len.to_i)
      song_data = decryptor.decrypt(encrypted_song_data)

      filename = "#{song_name_en.parameterize}-#{id}.mid"

      process_options = {
        write_file:       ENV['WRITE_FILE'].present? ? true : false,
        lang:             lang,
        filename:         filename,
        song_id:          id,
        song_name:        song_name,
        song_name_en:     song_name_en,
        song_name_search: song_name_search,
        song_data:        song_data
      }

      MidiSequence.process_midi_data(process_options)

      # song_file = File.new(Rails.root.join('lib', 'midi', 'extracted', lang, filename).to_s, 'wb+')
      # song_file.write(song_data)
      # song_file.close
    end

    data.close
  end
end
