# frozen_string_literal: true
namespace :social do # rubocop:disable BlockLength
  desc 'Post morning song'
  task post_morning_song: :environment do
    song = Song.where('views_count >= 20 AND code > 49999').order('RAND()').first

    message = "Bài Karaoke buổi sáng: #{song.name} (#{song.code})"
    link = "https://karaoke5so.com/karaoke/#{song.slug}"

    post_song(song, message, link)
  end

  desc 'Post noon song'
  task post_noon_song: :environment do
    song = Song.where('views_count >= 20 AND code > 49999').order('RAND()').first

    message = "Bài Karaoke buổi trưa: #{song.name} (#{song.code})"
    link = "https://karaoke5so.com/karaoke/#{song.slug}"

    post_song(song, message, link)
  end

  desc 'Post afternoon song'
  task post_afternoon_song: :environment do
    song = Song.where('views_count >= 20 AND code > 49999').order('RAND()').first

    message = "Bài Karaoke buổi chiều: #{song.name} (#{song.code})"
    link = "https://karaoke5so.com/karaoke/#{song.slug}"

    post_song(song, message, link)
  end

  desc 'Post night song'
  task post_night_song: :environment do
    song = Song.where('views_count >= 20 AND code > 49999').order('RAND()').first

    message = "Bài Karaoke buổi tối: #{song.name} (#{song.code})"
    link = "https://karaoke5so.com/karaoke/#{song.slug}"

    post_song(song, message, link)
  end

  def post_song(song, _message, link)
    new_message = build_message(song)

    fb_data = {
      access_token: ENV['FB_PAGE_ACCESS_TOKEN'],
      message: new_message,
      link: link
    }

    fb_response = HTTParty.post "#{ENV['FB_GRAPH_URL']}/#{ENV['FB_PAGE_ID']}/feed", body: fb_data
    Rails.logger.info "====> Posted to Facebook: #{song.name} - #{fb_response.body}"

    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_TOKEN_SECRET']
    end

    tweet = client.update("#{new_message} #{link}")

    Rails.logger.info "====> Posted to Twitter: #{song.name} - #{tweet}"
  end

  def build_message(song)
    question_text[rand(question_text.count)].gsub('{song}', "#{song.name} (#{song.code})")
  end

  def question_text
    [
      'Bạn có thích hát bài {song} không?',
      'Bạn nào đã từng hát bài {song} nè?',
      'Bạn đã hát bài {song} bao giờ chưa?',
      'Ai đã từng hát bài {song} điểm danh nào!',
      'Bài hát {song} có ai thích không nào?',
      'Có ai thích hát bài {song} không?',
      'Ai thích hát bài {song} thì comment nha!',
      'Có ai nhớ từng hát bài {song} không?',
      'Bạn hát bài {song} lần cuối hồi nào?',
      'Bạn hát bài {song} được bao nhiêu điểm?',
      'Ai hát bài {song} được 100 điểm nào?',
      'Có bạn nào biết bài {song} không?',
      'Ai hay hát bài {song} thì vô comment nà!'
    ]
  end
end
