# frozen_string_literal: true
namespace :unicode do
  desc 'Extract midi files'
  task build_vni_chars: :environment do
    uni_chars = []
    vni_chars = []

    File.read(Rails.root.join('lib', 'vni', 'vni_unicode.csv')).each_line do |line|
      uni_char, _v1, vni_char, _v2 = line.split(',')
      uni_chars << uni_char
      vni_chars << vni_char
    end

    p uni_chars.map { |char| "'#{char}'" }.join(', ')
    p vni_chars.map { |char| "'#{char}'" }.join(', ')
  end
end
