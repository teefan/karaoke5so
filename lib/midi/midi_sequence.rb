# frozen_string_literal: true
require 'midilib/io/seqreader'
require 'midilib/io/seqwriter'
require 'midilib/consts'
require 'midilib/utils'

require 'vni/vni_unicode_converter'

module MidiSequence
  def self.process_midi_data(options)
    Rails.logger.info "====> Processing song #{options[:song_name]}..."
    lyrics = ''
    VniUnicodeConverter.init_data

    seq = MIDI::Sequence.new
    seq.read(StringIO.new(options[:song_data])) do |track, _num_tracks, track_id|
      # Rails.logger.info "Reading track ##{track_id} of #{num_tracks} tracks"
      if track_id == 1 && track
        track.each do |event|
          process_info(options[:song_name], track_id, event)
        end
      end

      if track_id == 2 && track
        track.each do |event|
          lyrics = process_lyrics(track_id, event, lyrics)
        end
      end
    end

    save_song(seq, options.merge(extract_voice_options(lyrics)))
  end

  def self.extract_voice_options(lyrics)
    {
      lyrics:          lyrics,
      man_supported:   lyrics.match?(/@m/) ? true : false,
      woman_supported: lyrics.match?(/@w/) ? true : false
    }
  end

  def self.save_song(midi_sequence, options)
    song = Song.where(code: options[:song_id]).first_or_create
    song.update_attributes(formatted_attributes(options))

    return unless options[:write_file]

    File.open(Rails.root.join('lib', 'midi', 'extracted', options[:lang], options[:filename]).to_s, 'wb+') do |file|
      midi_sequence.write(file)
    end
  end

  def self.formatted_attributes(options)
    {
      name:            options[:song_name].mb_chars.upcase.strip.squeeze(' '),
      name_en:         options[:song_name_en].mb_chars.titleize.strip.squeeze(' '),
      name_s:          options[:song_name_search].mb_chars.downcase,
      lyrics:          options[:lyrics].mb_chars.gsub(/\# |@\w/, ''),
      man_supported:   options[:man_supported],
      woman_supported: options[:woman_supported]
    }
  end

  def self.process_info(song_name, _track_id, event)
    return event if event.class != MIDI::MetaEvent

    case event.meta_type
    when MIDI::META_SEQ_NAME
      event.data = song_name
    when MIDI::META_COPYRIGHT
      event.data = '© Teefan Inc.'
    end

    event
  end

  def self.process_lyrics(_track_id, event, lyrics = '')
    return lyrics if event.class != MIDI::MetaEvent

    full_lyrics = lyrics

    case event.meta_type
    when MIDI::META_LYRIC
      unicode_data_str = VniUnicodeConverter.convert(event.data_as_str)

      full_lyrics += unicode_data_str
    end

    full_lyrics
  end
end
