# frozen_string_literal: true
require 'test_helper'

class DataFilesControllerTest < ActionDispatch::IntegrationTest
  test 'should show data_file' do
    get data_file_url(id: '51297')

    assert_response :success
  end
end
