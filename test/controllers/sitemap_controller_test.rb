# frozen_string_literal: true
require 'test_helper'

class SitemapControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get songs_map_url(format: :xml)
    assert_response :success
  end
end
