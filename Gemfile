# frozen_string_literal: true
source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'dotenv-rails'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
gem 'rails-i18n', '~> 5.0.0'
# JSON generation
gem 'active_model_serializers', '~> 0.10.0'
# A set of responders modules
gem 'responders'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.18', '< 0.5'
# Use SQLite3 to read file database
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Semantic UI as CSS framework
gem 'semantic-ui-sass', git: 'https://github.com/doabit/semantic-ui-sass.git'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby
# Compile both digest and non-digest assets
gem 'non-stupid-digest-assets'

# Use FriendlyID to build permalink
gem 'friendly_id', '~> 5.2.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Pure Ruby implementation of the RC4 algorithm
gem 'ruby-rc4'
# Pure Ruby MIDI file and event manipulation library
gem 'midilib'
# Use Ransack as simple search interface
gem 'ransack'
# Use Kaminari for pagination
gem 'kaminari'
# Use Impressionist to log impressions per action or manually per model
gem 'impressionist'
# User Annotate to add a comment summarizing the current schema
gem 'annotate'

# Use Devise for authentication
gem 'devise'
gem 'devise-i18n'

# Use RailsAdmin to manage data.
# gem 'rails_admin', '~> 1.1.1'

# Use React as front-end framework
gem 'react-rails'

# Use HTTParty for external HTTP requests
gem 'httparty'
# Use Whenever gem to schedule job
gem 'whenever'
# Post Twitter update
gem 'twitter'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '~> 3.0.5'
  gem 'web-console', '>= 3.3.0'

  # Use Fuubar progress formatter
  gem 'fuubar', require: false
  # Progress format for Minitest
  gem 'minitest-reporters', require: false
  # Use Rubocop to enforce many of the guidelines outlined in the community Ruby Style Guide
  gem 'rubocop', '~> 0.47.1', require: false

  # Use Overcommit to manage and configure Git hooks
  gem 'overcommit', require: false

  # Use Capistrano for deployment
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-bundler', '~> 1.2'
  gem 'capistrano-passenger'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rvm'
end
