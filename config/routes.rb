# frozen_string_literal: true
Rails.application.routes.draw do
  # devise_for :users

  # support Facebook canvas post
  post '/' => 'songs#index'

  resources :data_files, path: :data, only: [:show]
  resources :songs_list, path: :karaoke_list, only: [:show]
  resources :songs, path: :karaoke, only: [:index, :show]

  get '/muoi_bai_nhieu_nguoi_hat_nhat' => 'songs#index', q: 'most_viewed', filter: 'most_viewed'
  get '/muoi_bai_karaoke_hay' => 'songs#index', q: { featured_eq: 1 }, filter: 'featured'
  get '/muoi_bai_karaoke_ngau_nhien' => 'songs#index', q: 'random', filter: 'random'
  get '/muoi_bai_karaoke_song_ca' => 'songs#index',
      q: { man_supported_and_woman_supported_eq: 1 }, filter: 'man_and_woman'
  get '/muoi_bai_karaoke_giong_nam' => 'songs#index', q: { man_supported_eq: 1, woman_supported_eq: 0 }, filter: 'man'
  get '/muoi_bai_karaoke_giong_nu' => 'songs#index', q: { woman_supported_eq: 1, man_supported_eq: 0 }, filter: 'woman'

  get '/songs_map' => 'sitemap#index', format: :xml

  root 'songs#index'
end
