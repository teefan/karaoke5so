# frozen_string_literal: true
# config valid only for current version of Capistrano
lock '3.7.2'

set :application, 'karaoke5so'
set :repo_url, 'git@bitbucket.org:teefan/karaoke5so.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/karaoke5so'

set :rvm_type, :auto                      # Defaults to: :auto
set :rvm_ruby_version, '2.4.0@karaoke5so' # Defaults to: 'default'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, '.env', 'config/database.yml', 'config/secrets.yml', 'lib/midi/midi.dat', 'lib/midi/midi.db'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'public/uploads',
       'public/soundfonts', 'public/impulses', 'public/pat', 'lib/midi/extracted'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :passenger_restart_with_touch, false # Note that `nil` is NOT the same as `false` here

set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }
