# frozen_string_literal: true
# Where the I18n library should search for translation files
I18n.load_path += Dir[Rails.root.join('lib', 'locale', '*.{rb,yml}')]

# Set available locales
I18n.available_locales = [:en, :vi]

# Set default locale to something other than :en
I18n.default_locale = :vi
