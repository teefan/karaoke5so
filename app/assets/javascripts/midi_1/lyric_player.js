if (typeof MIDI === 'undefined') MIDI = {};
if (typeof MIDI.LyricPlayer === 'undefined') MIDI.LyricPlayer = {};

(function() {
  'use strict';

  var midiLyric         = MIDI.LyricPlayer;
  midiLyric.currentTime = 0;
  midiLyric.endTime     = 0;
  midiLyric.restart     = 0;
  midiLyric.playing     = false;
  midiLyric.timeWarp    = 1;
  midiLyric.startDelay  = 0;
  midiLyric.BPM         = 65;

  midiLyric.start =
    midiLyric.resume = function(onsuccess) {
      if (midiLyric.currentTime < -1) {
        midiLyric.currentTime = -1;
      }
      return startLyric(midiLyric.currentTime, onsuccess);
    };

  midiLyric.pause = function() {
    var tmp = midiLyric.restart;
    stopLyrics();
    midiLyric.restart = tmp;
  };

  midiLyric.stop = function() {
    stopLyrics();
    midiLyric.restart     = 0;
    midiLyric.currentTime = 0;
  };

  midiLyric.addListener = function(onsuccess) {
    onMidiEvent = onsuccess;
  };

  midiLyric.removeListener = function() {
    onMidiEvent = undefined;
  };

  midiLyric.clearAnimation = function() {
    if (midiLyric.animationFrameId) {
      cancelAnimationFrame(midiLyric.animationFrameId);
    }
  };

  midiLyric.setAnimation = function(callback) {
    var currentTime = 0;
    var tOurTime    = 0;
    var tTheirTime  = 0;
    //
    midiLyric.clearAnimation();
    ///
    var frame = function() {
      midiLyric.animationFrameId = requestAnimationFrame(frame);
      ///
      if (midiLyric.endTime === 0) {
        return;
      }
      if (midiLyric.playing) {
        currentTime = (tTheirTime === midiLyric.currentTime) ? tOurTime - Date.now() : 0;
        if (midiLyric.currentTime === 0) {
          currentTime = 0;
        } else {
          currentTime = midiLyric.currentTime - currentTime;
        }
        if (tTheirTime !== midiLyric.currentTime) {
          tOurTime   = Date.now();
          tTheirTime = midiLyric.currentTime;
        }
      } else { // paused
        currentTime = midiLyric.currentTime;
      }
      ///
      var endTime = midiLyric.endTime;
      var percent = currentTime / endTime;
      var total   = currentTime / 1000;
      var minutes = total / 60;
      var seconds = total - (minutes * 60);
      var t1      = minutes * 60 + seconds;
      var t2      = (endTime / 1000);
      ///
      if (t2 - t1 < -1.0) {
        return;
      } else {
        callback({
          now:    t1,
          end:    t2,
          events: []
        });
      }
    };
    ///
    requestAnimationFrame(frame);
  };

  midiLyric.loadFileData = function(data, onsuccess, onprogress, onerror) {
    midiLyric.currentData = window.atob(data);
    try {
      midiLyric.replayer = new Replayer(MidiFile(midiLyric.currentData), midiLyric.timeWarp, null, null);
      midiLyric.data     = midiLyric.replayer.getData();
      midiLyric.endTime  = getLength();
      onsuccess(midiLyric);
    } catch (event) {
      onerror && onerror(event);
    }
  };

  // Playing lyrics
  var getContext = function() {
    midiLyric.ctx = { currentTime: 0 };
    return midiLyric.ctx;
  };

  var getLength = function() {
    var data      = midiLyric.data;
    var length    = data.length;
    var totalTime = 0.5;
    for (var n = 0; n < length; n++) {
      totalTime += data[n][1];
    }
    return totalTime;
  };

  var __now;
  var getNow = function() {
    if (window.performance && window.performance.now) {
      return window.performance.now();
    } else {
      return Date.now();
    }
  };

  var onMidiEvent = undefined;
  var queuedTime;
  var startTime   = 0; // to measure time elapse

  var startLyric = function(currentTime, onsuccess) {
    if (!midiLyric.replayer) {
      return;
    }
    if (typeof currentTime === 'undefined') {
      currentTime = midiLyric.restart;
    }
    midiLyric.playing && stopLyrics();
    midiLyric.playing = true;
    midiLyric.data    = midiLyric.replayer.getData();
    midiLyric.endTime = getLength();
    ///
    var offset   = 0;
    var data     = midiLyric.data;
    var ctx      = getContext();
    var length   = data.length;
    //
    queuedTime   = 0.5;
    ///
    var foffset  = currentTime - midiLyric.currentTime;
    ///
    // Teefan: time increased bug - what is now for?
    // var now         = getNow();
    // __now           = __now || now;
    // ctx.currentTime = (now - __now) / 1000;
    ///
    startTime = ctx.currentTime;

    // var time_debug = {
    //   currentTime: currentTime,
    //   'ctx.currentTime': ctx.currentTime,
    //   'midiLyric.currentTime': midiLyric.currentTime,
    //   foffset: foffset
    // };
    //
    // console.log(time_debug);

    var lyrics = [];
    for (var n = 0; n < length; n++) {
      var obj = data[n];
      if ((queuedTime += obj[1]) <= currentTime) {
        offset = queuedTime;
        continue;
      }
      ///
      currentTime = queuedTime - offset;
      ///
      var event = obj[0].event;

      // if (event.type !== 'meta' || event.subtype !== 'lyrics') {
      //   continue;
      // }

      var channelId = event.channel;
      var channel   = MIDI.channels[channelId];
      var delay     = ctx.currentTime + ((currentTime + foffset + midiLyric.startDelay) / 1000);
      var queueTime = queuedTime - offset + midiLyric.startDelay;

      if (event.subtype === 'lyrics') {
        var lyricEvent = { queueTime: queueTime, delay: delay, text: event.text };
        lyrics.push(lyricEvent);
      }
    }
    ///
    onsuccess && onsuccess(lyrics);
    return lyrics;
  };

  var stopLyrics = function() {
    var ctx = getContext();
    midiLyric.playing = false;
    midiLyric.restart += (ctx.currentTime - startTime) * 1000;
  };

})();
