(function() {
  // Patch everything onto module
  var global = window;

  // From: http://midijs.net/lib/midi.js

  // MIDI.js
  //
  // 100% JavaScript MIDI File Player using W3C Web Audio API
  // with fallbacks for older browsers
  //
  // Copyrights Johannes Feulner, Karlsruhe, Germany, 2014. All rights reserved.
  // Contact: weblily@weblily.net
  //

  var audioMethod;
  var context         = 0;
  var source          = 0;
  var audioBufferSize = 8192;
  var waveBuffer;
  var midiFileBuffer;
  var read_wave_bytes = 0;
  var song            = 0;
  var midijs_url      = '/';
  var start_time      = 0;
  var audio_status    = '';

  function require_script(file, callback) {

    var script = document.getElementsByTagName('script')[0],
        newjs  = document.createElement('script');

    // IE
    newjs.onreadystatechange = function() {
      if (newjs.readyState === 'loaded' || newjs.readyState === 'complete') {
        newjs.onreadystatechange = null;
        callback();
      }
    };

    // others
    newjs.onload = function() {
      callback();
    };

    newjs.onerror = function() {
      MIDIjs.messageCallback('Error: Cannot load  JavaScript file ' + file);
    };

    newjs.src  = file;
    newjs.type = 'text/javascript';
    script.parentNode.insertBefore(newjs, script);
  }

  function get_next_wave(ev) {
    var player_event  = {};
    player_event.time = context.currentTime - start_time;
    MIDIjs.playerCallback(player_event);
    MIDIjs.eventCallback({ type: 'player_status', status: 'playing', data: player_event});
    // collect new wave data from libtimidity into waveBuffer
    read_wave_bytes = MIDIAudio.ccall('mid_song_read_wave', 'number',
      ['number', 'number', 'number', 'number'],
      [song, waveBuffer, audioBufferSize * 2, false]);
    if (0 === read_wave_bytes) {
      stop_WebAudioAPI();
      return;
    }

    var max_i16 = Math.pow(2, 15);
    for (var i = 0; i < audioBufferSize; i++) {
      if (i < read_wave_bytes) {
        // convert PCM data from C sint16 to JavaScript number (range -1.0 .. +1.0)
        ev.outputBuffer.getChannelData(0)[i] = MIDIAudio.getValue(waveBuffer + 2 * i, 'i16') / max_i16;
      } else {
        MIDIjs.messageCallback('Filling 0 at end of buffer');
        ev.outputBuffer.getChannelData(0)[i] = 0;   // fill end of buffer with zeroes, may happen at the end of a piece
      }
    }
  }

  function loadMissingPatch(path, filename) {
    var request = new XMLHttpRequest();
    request.open('GET', path + filename, true);
    request.responseType = 'arraybuffer';

    request.onerror = function() {
      MIDIjs.messageCallback('Error: Cannot retrieve patch file ' + path + filename);
    };

    request.onload = function() {
      if (200 !== request.status) {
        MIDIjs.messageCallback('Error: Cannot retrieve patch file ' + path + filename + ' : ' + request.status);
        return;
      }

      num_missing--;
      MIDIAudio.FS_createDataFile('pat/', filename, new Int8Array(request.response), true, true);
      MIDIjs.messageCallback('Loading instruments: ' + num_missing);
      MIDIjs.eventCallback({
        type: 'instruments_load',
        data: { total: total_instruments, loaded: total_instruments - num_missing }
      });
      if (num_missing === 0) {
        stream               = MIDIAudio.ccall('mid_istream_open_mem', 'number',
          ['number', 'number', 'number'],
          [midiFileBuffer, midiFileArray.length, false]);
        var MID_AUDIO_S16LSB = 0x8010; // signed 16-bit samples
        var options          = MIDIAudio.ccall('mid_create_options', 'number',
          ['number', 'number', 'number', 'number'],
          [context.sampleRate, MID_AUDIO_S16LSB, 1, audioBufferSize * 2]);
        song                 = MIDIAudio.ccall('mid_song_load', 'number', ['number', 'number'], [stream, options]);
        rval                 = MIDIAudio.ccall('mid_istream_close', 'number', ['number'], [stream]);
        MIDIAudio.ccall('mid_song_start', 'void', ['number'], [song]);

        // create script Processor with buffer of size audioBufferSize and a single output channel
        source                = context.createScriptProcessor(audioBufferSize, 0, 1);
        waveBuffer            = MIDIAudio._malloc(audioBufferSize * 2);
        source.onaudioprocess = get_next_wave;        // add eventhandler for next buffer full of audio data
        source.connect(context.destination);          // connect the source to the context's destination (the speakers)
        start_time = context.currentTime;
        MIDIjs.messageCallback('Playing: ...');
        MIDIjs.eventCallback({ type: 'player_status', status: 'start_playing', data: { time: 0 } });
      }
    };
    request.send();
  }

  function unmute_iOS_hack() {
    // iOS unmutes web audio when playing a buffer source
    // the buffer may be initialized to all zeroes (=silence)
    var sinusBuffer = context.createBuffer(1, 44100, 44100);
    freq            = 440; // Hz
    for (i = 0; i < 48000; i++) {
      sinusBuffer.getChannelData(0)[i] = 0; // Math.sin(i / 48000 * 2 * Math.PI * freq);
    }
    var bufferSource    = context.createBufferSource();    // creates a sound source
    bufferSource.buffer = sinusBuffer;
    bufferSource.connect(context.destination);          // connect the source to the context's destination (the speakers)
    bufferSource.start(0); // play the bufferSource now
  }

  function play_WebAudioAPI(url) {
    stop_WebAudioAPI();
    play_WebAudioAPI_with_script_loaded(url);
  }

  function play_WebAudioAPIString(string) {
    // debugger;
    stop_WebAudioAPI();
    var buf     = new ArrayBuffer(string.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = string.length; i < strLen; i++) {
      bufView[i] = string.charCodeAt(i);
    }
    playTimidity(buf);
  }

  function playTimidity(string) {
    midiFileArray  = new Int8Array(string);
    midiFileBuffer = MIDIAudio._malloc(midiFileArray.length);
    MIDIAudio.writeArrayToMemory(midiFileArray, midiFileBuffer);

    rval                 = MIDIAudio.ccall('mid_init', 'number', [], []);
    stream               = MIDIAudio.ccall('mid_istream_open_mem', 'number',
      ['number', 'number', 'number'],
      [midiFileBuffer, midiFileArray.length, false]);
    var MID_AUDIO_S16LSB = 0x8010; // signed 16-bit samples
    var options          = MIDIAudio.ccall('mid_create_options', 'number',
      ['number', 'number', 'number', 'number'],
      [context.sampleRate, MID_AUDIO_S16LSB, 1, audioBufferSize * 2]);
    song                 = MIDIAudio.ccall('mid_song_load', 'number', ['number', 'number'], [stream, options]);
    rval                 = MIDIAudio.ccall('mid_istream_close', 'number', ['number'], [stream]);

    num_missing = MIDIAudio.ccall('mid_song_get_num_missing_instruments', 'number', ['number'], [song]);
    total_instruments = num_missing;
    MIDIjs.eventCallback({ type: 'instruments_load', data: { total: total_instruments, loaded: 0 } });
    if (0 < num_missing) {
      for (var i = 0; i < num_missing; i++) {
        var missingPatch = MIDIAudio.ccall('mid_song_get_missing_instrument', 'string', ['number', 'number'], [song, i]);
        loadMissingPatch(midijs_url + 'pat/', missingPatch);
      }
    } else {
      MIDIAudio.ccall('mid_song_start', 'void', ['number'], [song]);
      // create script Processor with auto buffer size and a single output channel
      source                = context.createScriptProcessor(audioBufferSize, 0, 1);
      waveBuffer            = MIDIAudio._malloc(audioBufferSize * 2);
      source.onaudioprocess = get_next_wave;    // add eventhandler for next buffer full of audio data
      source.connect(context.destination);      // connect the source to the context's destination (the speakers)
      start_time = context.currentTime;
      MIDIjs.messageCallback('Playing: ...');
      MIDIjs.eventCallback({ type: 'player_status', status: 'start_playing', data: { time: 0 } });
    }
  }

  function play_WebAudioAPI_with_script_loaded(url) {
    // Download url from server, url must honor same origin restriction
    MIDIjs.messageCallback('Loading MIDI file ' + url + ' ...');
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    request.onerror = function() {
      MIDIjs.messageCallback('Error: Cannot retrieve MIDI file ' + url);
    };

    request.onload = function() {
      if (200 !== request.status) {
        MIDIjs.messageCallback('Error: Cannot retrieve MIDI file ' + url + ' : ' + request.status);
        return;
      }

      MIDIjs.messageCallback('MIDI file loaded: ' + url);

      playTimidity(request.response);
    };
    request.send();
  }

  function stop_WebAudioAPI() {
    if (source) {
      // terminate playback
      source.disconnect();

      // hack: without this, Firfox 25 keeps firing the onaudioprocess callback
      source.onaudioprocess = 0;

      source = 0;

      // free libtimitdiy ressources
      MIDIAudio._free(waveBuffer);
      MIDIAudio._free(midiFileBuffer);
      MIDIAudio.ccall('mid_song_free', 'void', ['number'], [song]);
      song = 0;
      MIDIAudio.ccall('mid_exit', 'void', [], []);
      source = 0;
    }
    MIDIjs.messageCallback(audio_status);
    var player_event  = {};
    player_event.time = 0;
    MIDIjs.playerCallback(player_event);
    MIDIjs.eventCallback({ type: 'player_status', status: 'stop_playing', data: player_event });
  }

  function play_bgsound(url) {
    stop_bgsound();

    var sounddiv = document.getElementById('karaoke5soMIDI');
    if (!sounddiv) {
      sounddiv = document.createElement('div');
      sounddiv.setAttribute('id', 'karaoke5soMIDI');

      // hack: without the nbsp or some other character the bgsound will not be inserted
      sounddiv.innerHTML = '&nbsp;<bgsound src="' + url + '" volume="100"/>';
      document.body.appendChild(sounddiv);
    } else {
      sounddiv.lastChild.setAttribute('src', url);
    }
    source = sounddiv;
    MIDIjs.messageCallback('Playing ' + url + ' ...');
  }

  function stop_bgsound() {
    if (source) {
      var sounddiv = source;
      sounddiv.lastChild.setAttribute('src', 'midi/silence.mid');
      source = 0;
    }
    MIDIjs.messageCallback(audio_status);
  }

  function play_object(url) {
    stop_object();

    var sounddiv = document.getElementById('karaoke5soMIDI');
    if (!sounddiv) {
      sounddiv = document.createElement('div');
      sounddiv.setAttribute('id', 'karaoke5soMIDI');

      sounddiv.innerHTML = '<object data="' + url + '" autostart="true" volume="100" type="audio/mid"></object>';
      document.body.appendChild(sounddiv);
    } else {
      sounddiv.lastChild.setAttribute('data', url);
    }
    source = sounddiv;
    MIDIjs.messageCallback('Playing ' + url + ' ...');
  }

  function stop_object() {
    if (source) {
      var sounddiv = source;

      sounddiv.parentNode.removeChild(sounddiv);
      source = 0;
    }
    MIDIjs.messageCallback(audio_status);
  }

  audioMethod = 'none';

  if (bowser.msie) {
    audioMethod = 'bgsound';
  }

  window.AudioContext = window.AudioContext || window.webkitAudioContext;

  if (AudioContext) {
    context     = new AudioContext();
    audioMethod = 'WebAudioAPI';
  }

  global.MIDIjs = {};
  var MIDIjs    = global.MIDIjs;

  // default: write messages to browser console
  global.MIDIjs.messageCallback = function(message) {
    // console.log('message: ', message);
  };
  global.MIDIjs.playerCallback  = function(playerEvent) {
    // console.log('playerEvent: ', playerEvent);
  };
  global.MIDIjs.eventCallback    = function(loadEvent) {
    // console.log('loadEvent: ', loadEvent);
  };
  global.MIDIjs.getAudioStatus  = function() {
    return audio_status;
  };

  global.MIDIjs.unmute_iOS_hack = unmute_iOS_hack;

  if (audioMethod === 'WebAudioAPI') {
    global.MIDIjs.play       = play_WebAudioAPI;
    global.MIDIjs.playString = play_WebAudioAPIString;
    global.MIDIjs.stop       = stop_WebAudioAPI;
    audio_status             = 'audioMethod: WebAudioAPI' +
      ', sampleRate (Hz): ' + context.sampleRate +
      ', audioBufferSize (Byte): ' + audioBufferSize;
  } else if (audioMethod === 'bgsound') {
    global.MIDIjs.play = play_bgsound;
    global.MIDIjs.stop = stop_bgsound;
    audio_status       = 'audioMethod: &lt;bgsound&gt;';
  } else if (audioMethod === 'object') {
    global.MIDIjs.play = play_object;
    global.MIDIjs.stop = stop_object;
    audio_status       = 'audioMethod: &lt;object&gt;';
  } else {
    global.MIDIjs.play = function(url) {};
    global.MIDIjs.stop = function(url) {};
    audio_status       = 'audioMethod: No method found';
  }
})();
