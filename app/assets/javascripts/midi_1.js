//= require shim/Base64
//= require shim/Base64binary
//= require shim/WebAudioAPI

//= require jasmid/stream
//= require jasmid/midifile
//= require jasmid/replayer

//= require midi_1/audioDetect
//= require midi_1/gm
//= require midi_1/loader
//= require midi_1/plugin.audiotag
//= require midi_1/plugin.webaudio
//= require midi_1/plugin.webmidi
//= require midi_1/player
//= require midi_1/lyric_player

//= require util/dom_request_xhr
//= require util/dom_request_script
