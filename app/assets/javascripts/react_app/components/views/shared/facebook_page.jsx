const FacebookPage = (props) => {
  return (
    <div
      className="fb-page"
      data-href="https://www.facebook.com/karaoke5so"
      data-small-header="true"
      data-tabs="timeline"
      data-adapt-container-width="true"
      data-hide-cover="false"
      data-show-facepile="true"
      data-height="auto"
    >
      <blockquote cite="https://www.facebook.com/karaoke5so" className="fb-xfbml-parse-ignore">
        <a href="https://www.facebook.com/karaoke5so">Hát Karaoke 5 số Online Community</a>
      </blockquote>
    </div>
  );
};
