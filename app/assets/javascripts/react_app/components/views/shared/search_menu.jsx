class SearchMenu extends React.Component {
  constructor(props) {
    super(props);

    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    $('.ui.search').search({
      // change search endpoint to a custom endpoint by manipulating apiSettings
      apiSettings: {
        onSearchQuery: (query) => {
          return query.trim();
        },
        url: '/karaoke.json?q[name_en_cont]={query}&q[name_s_start]={query}&q[code_start]={query}&q[m]=or',
        onResponse: function(searchResponse) {
          let response = {
            results: []
          };

          for (let i in searchResponse) {
            let item = searchResponse[i];

            let lyricsPreview = '';

            if (item.lyrics && item.lyrics.length > 0) {
              lyricsPreview = ` - ${item.lyrics.substring(0, 60)}...`;
            }

            response.results.push({
              title:       item.name,
              description: `${item.code}${lyricsPreview}`,
              url:         `/karaoke/${item.slug}`
            });
          }

          return response;
        }
      },
      fields: {
        results: 'results',
        title: 'title'
      },
      searchDelay: 800,
      minCharacters : 2,
      maxResults: 10,
      type: 'standard',
      error : {
        source      : 'Cannot search. No source used, and Semantic API module was not included',
        noResults   : 'Tìm kiếm không có kết quả',
        logging     : 'Error in debug logging, exiting.',
        noTemplate  : 'A valid template name was not specified.',
        serverError : 'There was an issue with querying the server.',
        maxResults  : 'Results must be an array to use maxResults setting',
        method      : 'The method you called is not defined.'
      }
    });
  }

  render() {
    return (
      <div className="ui search">
        <div className="ui icon input" style={{width: '100%'}}>
          <input className="prompt" type="text" placeholder="Tìm kiếm: tên bài hát hoặc mã số karaoke" />
          <i className="search icon" />
        </div>
        <div className="results"></div>
      </div>
    );
  }
}
