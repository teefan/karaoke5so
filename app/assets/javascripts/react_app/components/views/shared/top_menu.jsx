class TopMenu extends React.Component {
  constructor(props) {
    super(props);

    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    // $('.ui.dropdown#songs-list').dropdown();
    // $('.ui.dropdown#top-right-menu').dropdown();
  }

  render() {
    let { currentUser } = this.props;

    let rightMenu =
      <div className="right menu">
        <div id="top-right-menu" className="ui simple dropdown icon item">
          <i className="ellipsis vertical icon" />
          <div className="menu">
            <a className="item" href="/">Menu</a>
          </div>
        </div>
      </div>;

    return (
      <div className="ui big inverted menu">
        <a className="header item" href="/">Karaoke 5 số</a>
        <div id="songs-list" className="ui simple dropdown link item">
          <span className="text">Ngẫu nhiên</span>
          <i className="dropdown icon" />
          <div className="menu">
            <a className="item" href="/muoi_bai_nhieu_nguoi_hat_nhat">Hát Nhiều Gần Đây</a>
            <a className="item" href="/muoi_bai_karaoke_hay">Tuyển Chọn</a>
            <a className="item" href="/muoi_bai_karaoke_ngau_nhien">Tất Cả</a>
            <a className="item" href="/muoi_bai_karaoke_song_ca">Song Ca</a>
            <a className="item" href="/muoi_bai_karaoke_giong_nam">Giọng Nam</a>
            <a className="item" href="/muoi_bai_karaoke_giong_nu">Giọng Nữ</a>
            <a className="item disabled" href="/">Yêu Thích</a>
          </div>
        </div>
      </div>
    );
  }
}

TopMenu.propTypes = {
  currentUser: React.PropTypes.object
};
