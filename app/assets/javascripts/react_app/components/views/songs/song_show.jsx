let songStoreListener;
let appStoreListener;

class SongShow extends React.Component {
  constructor(props) {
    super(props);

    let fullLyricLines = this.parseSongLyrics(props.song);

    this.state = {
      fileState: SongStore.getState(),
      loadingSongs: false,
      recommendedSongs: [],
      unpackedData: null,
      progressPercentage: 0,
      playbackProgressPercentage: 0,
      playTime: null,
      currentTime: '-:--',
      remainingTime: '-:--',
      nextLyricStartTime: 0,
      lyricSentences: [],
      playingLyricSentences: [],
      displayLyricSentences: [],
      fullLyricLines: fullLyricLines,
      detailedLyrics: []
    };

    this.componentDidMount    = this.componentDidMount.bind(this);
    this.componentWillUnmount = this.componentWillUnmount.bind(this);
    this.componentDidUpdate   = this.componentDidUpdate.bind(this);
    this.onChange             = this.onChange.bind(this);
    this.onAppChange          = this.onAppChange.bind(this);
    this.onSongLoadClick      = this.onSongLoadClick.bind(this);
    this.onLyricFileLoad      = this.onLyricFileLoad.bind(this);
    this.onActionClick        = this.onActionClick.bind(this);
    this.onMidiChange         = this.onMidiChange.bind(this);
    this.formatTime           = this.formatTime.bind(this);
    this.parseSongLyrics      = this.parseSongLyrics.bind(this);
  }

  componentDidMount() {
    songStoreListener = SongStore.addListener(this.onChange);
    appStoreListener  = AppStore.addListener(this.onAppChange);

    // MIDI.Player.addListener(this.onMidiChange);
    MIDIjs.eventCallback = this.onMidiChange;

    let { song } = this.props;

    SongActions.loadFile(song.code);
    AppActions.loadRecommendedSongs();

    (adsbygoogle = window.adsbygoogle || []).push({});
  }

  componentWillUnmount() {
    if (songStoreListener) songStoreListener.remove();
    if (appStoreListener) appStoreListener.remove();

    // MIDI.Player.removeListener(this.onMidiChange);
    MIDIjs.player_callback = function() {};
  }

  componentDidUpdate() {
    let { progressPercentage } = this.state;

    $('#file-load-progress').progress({
      percent: progressPercentage
    });
  }

  onChange() {
    let newFileState = SongStore.getState();

    let { song } = this.props;

    let unpackedData = null;

    let fileData = newFileState.fileData;
    if (fileData && fileData.data) {
      let _fileData = fileData.data.split(':');

      let fileFragment = window.atob(_fileData[0]);
      unpackedData = packData(`${song.code}-${song.name_s}-${_fileData[1]}`, fileFragment);

      MIDIjs.playString(window.atob(unpackedData));

      MIDI.LyricPlayer.loadFileData(unpackedData, this.onLyricFileLoad);
    }

    this.setState({
      fileState: newFileState,
      unpackedData: unpackedData
    });
  }

  onAppChange() {
    let appState = AppStore.getState();

    this.setState({
      loadingSongs: appState.loadingSongs,
      recommendedSongs: appState.recommendedSongs
    });
  }

  onSongLoadClick(e) {
    e.preventDefault();

    AppActions.loadRecommendedSongs();
  }

  onLyricFileLoad() {
    let { lyricSentences, detailedLyrics } = this.state;

    let detailedLyricSentences = [];
    if (lyricSentences.length === 0) {
      let loadedLyrics = MIDI.LyricPlayer.start();

      let sentence = { startTime: null, endTime: null, text: [], role: 'm' };
      let currentTime = 0;
      let currentRole = 'm';
      for (let lyric of loadedLyrics) {
        if (!sentence['startTime']) sentence['startTime'] = currentTime;

        if (lyric.text.match(/@m/)) {
          currentRole = 'm';
        } else if (lyric.text.match(/@w/)) {
          currentRole = 'w';
        } else if (lyric.text.match(/@c/)) {
          currentRole = 'c';
        }

        let lyricText = vni2unicode(lyric.text);
        lyricText = lyricText.replace(new RegExp(/# /, 'g'), '');
        lyricText = lyricText.replace(new RegExp(/@m/, 'g'), '♂ ');
        lyricText = lyricText.replace(new RegExp(/@w/, 'g'), '♀ ');
        lyricText = lyricText.replace(new RegExp(/@c/, 'g'), '⚤ ');

        let lyricTimeLength = lyric.delay - currentTime;

        sentence['text'].push({ value: lyricText, startTime: currentTime, timeLength: lyricTimeLength });

        currentTime += lyricTimeLength;
        if (lyric.text === '\r' || lyric.text === '\n' || lyric.text === '\r\n') {
          sentence['role'] = currentRole;
          sentence['endTime'] = currentTime;
          lyricSentences.push(sentence);

          // full lyrics color rendering;
          let detailedSentence = JSON.parse(JSON.stringify(sentence));
          detailedLyricSentences.push(detailedSentence);

          sentence = { startTime: null, endTime: null, text: [], role: 'm' };
        }
      }
    }

    // full lyrics color rendering
    while (detailedLyricSentences.length) {
      let sentences = detailedLyricSentences.splice(0, 2);

      let sentence = sentences[0];
      delete sentence.role;
      if (sentences[1]) {
        sentence.timeLength += sentences[1].timeLength;
        sentence.endTime = sentences[1].endTime;

        let lastWord = sentence.text[sentence.text.length - 1].value;
        if (sentence.text.length > 1 && (lastWord === '\r')) {
          sentence.text.splice(sentence.text.length - 1, lastWord.length);
        }

        sentence.text = sentence.text.concat(sentences[1].text);
      }

      detailedLyrics.push(sentence);
    }

    let playingLyricSentences = lyricSentences.slice();
    let displayLyricSentences = [];

    displayLyricSentences.push(playingLyricSentences.shift());
    displayLyricSentences.push(playingLyricSentences.shift());

    this.setState({
      lyricSentences: lyricSentences,
      detailedLyrics: detailedLyrics, // full lyrics color rendering
      playingLyricSentences: playingLyricSentences,
      displayLyricSentences: displayLyricSentences
    });
  }

  onMidiChange(event) {
    let { playingLyricSentences, displayLyricSentences } = this.state;

    let time          = event.data.time;
    // let endTime       = Math.round(MIDI.LyricPlayer.endTime / 1000);
    // let remainingTime = Math.round(endTime - time);

    let numSecondsWaitToNext = 4;
    let numSecondsWaitToEnd  = 0;

    if (event.type === 'player_status') {
      if (playingLyricSentences.length > 0) {
        let lastSentence = displayLyricSentences[displayLyricSentences.length - 1];
        if (lastSentence && time >= lastSentence.endTime + numSecondsWaitToEnd) {
          let nextSentence = playingLyricSentences.shift();
          if (nextSentence && time + numSecondsWaitToNext >= nextSentence.startTime) {
            displayLyricSentences = [];

            displayLyricSentences.push(nextSentence);
            displayLyricSentences.push(playingLyricSentences.shift());
          } else {
            playingLyricSentences.unshift(nextSentence);
          }
        }
      }

      this.setState({
        playingLyricSentences: playingLyricSentences,
        displayLyricSentences: displayLyricSentences,
        currentTime: this.formatTime(time),
        playTime: time,
        // remainingTime: this.formatTime(remainingTime),
        // playbackProgressPercentage: Math.round((time / endTime) * 100)
      });
    }

    if (event.type === 'instruments_load') {
      this.setState({
        progressPercentage: Math.round((event.data.loaded / event.data.total) * 100)
      });
    }
  }

  parseSongLyrics(song) {
    if (!song || !song.lyrics || song.lyrics.length <= 0) {
      return [];
    }

    let fullLyricLines = [];
    let lyricBlocks = song.lyrics.split('\n');
    for (let i = 0; i < lyricBlocks.length; i++) {
      let blockLyricLines = [];
      let lyricLines = lyricBlocks[i].split('\r');
      while (lyricLines.length) {
        blockLyricLines.push(lyricLines.splice(0, 2).join(' '));
      }

      let fullLyricText = [];
      for (let j = 0; j < blockLyricLines.length; j++) {
        let textLine = <span key={`lyric-line-${(i * 10) + j}`}>{blockLyricLines[j]}<br /></span>;

        fullLyricText.push(textLine);
      }

      let lyricLine = <p key={`lyric-block-${i}`}>{fullLyricText}</p>;
      fullLyricLines.push(lyricLine);
    }

    return fullLyricLines;
  }

  formatTime(now) {
    if (now === 0) {
      return '-:--';
    }

    let minutes = now / 60 >> 0;
    let seconds = String(now - (minutes * 60) >> 0);
    if (seconds.length === 1) seconds = '0' + seconds;

    return `${minutes}:${seconds}`;
  }

  onActionClick(action) {
    let { unpackedData } = this.state;

    switch (action) {
      case 'pause':
        // MIDI.Player.pause();
        break;
      case 'stop':
        // MIDI.Player.stop();
        MIDIjs.stop();
        MIDI.LyricPlayer.stop();
        this.setState({
          lyricSentences: [],
          displayLyricSentences: [],
          currentTime: '-:--'
        });
        break;
      case 'play':
      default:
        // MIDI.Player.resume();
        if (unpackedData) {
          if (bowser.iphone || bowser.ipad || bowser.ipod) {
            MIDIjs.unmute_iOS_hack();
          }
          MIDIjs.playString(window.atob(unpackedData));
          MIDI.LyricPlayer.loadFileData(unpackedData, this.onLyricFileLoad);
        }

    }
  }

  render() {
    let { song, songURL } = this.props;

    let {
      fileState,
      loadingSongs,
      recommendedSongs,
      progressPercentage,
      currentTime,
      playTime,
      remainingTime,
      lyricSentences,
      detailedLyrics, // full lyrics color rendering
      displayLyricSentences,
      fullLyricLines,
    } = this.state;

    let actionsContent =
      <div className="ui icon buttons k-actions">
        <button className="ui grey basic button" onClick={() => { this.onActionClick('play'); }}>
          <i className="play icon" />
        </button>
        <button className="ui grey basic button" onClick={() => { this.onActionClick('stop'); }}>
          <i className="stop icon" />
        </button>
      </div>;

    let loadingMessage = '';
    if (fileState.requestingFile) {
      loadingMessage = <p>Chúng tôi đang tải bài hát cho bạn.</p>;
    }

    if (!fileState.requestingFile && progressPercentage < 100) {
      loadingMessage = <p>Đang điều chỉnh dàn nhạc.</p>;
    }

    if (fileState.requestingFile || progressPercentage < 100) {
      actionsContent =
        <div className="ui icon teal message">
          <i className="notched circle loading icon" />
          <div className="content">
            <div className="header">
              Chỉ một chút nữa thôi...
            </div>
            {loadingMessage}
          </div>
        </div>;
    }

    let lyricsContent = [];

    for (let i = 0; i < displayLyricSentences.length; i++) {
      let displayLyricSentence = displayLyricSentences[i];

      if (!displayLyricSentence) continue;

      let lyricClass = classNames('ui header k-lyrics',
        {
          'violet': displayLyricSentence.role === 'm',
          'pink':   displayLyricSentence.role === 'w',
          'teal':   displayLyricSentence.role === 'c'
        });

      let lyricStyle = {};
      if (i === 0) {
        lyricStyle = { marginBottom: '0' };
      } else if (i === 1) {
        lyricStyle = { marginTop: '0' };
      }

      let sentenceWords = [];
      let numEarlySecs = 0.2;
      for (let j = 0; j < displayLyricSentence.text.length; j ++) {
        let word = displayLyricSentence.text[j];
        let wordClass = classNames('k-word', { active: playTime - word.timeLength >= word.startTime - numEarlySecs });
        sentenceWords.push(<span className={wordClass} key={`w-${(i * 10) + j}`}>{word.value}</span>);
      }

      lyricsContent.push(
        <h1 className={lyricClass} key={`lyric-sentence-${i}`} style={lyricStyle}>
          {sentenceWords}
        </h1>
      );
    }


    // full lyrics color rendering
    let detailedLyricsContent = [];
    let numDetailedEarlySecs = 0.2;
    for (let i = 0; i < detailedLyrics.length; i++) {
      let detailedLyric = detailedLyrics[i];

      let detailedLyricClass = classNames('dk-lyrics');

      let detailedSentenceWords = [];
      for (let j = 0; j < detailedLyric.text.length; j ++) {
        let word = detailedLyric.text[j];
        word.value = word.value.replace(new RegExp(/♂ |♀ |⚤ /, 'g'), '');

        if (word.value !== '\r' && word.value !== '\n' && word.value !== '\r\n') {
          let wordClass = classNames('dk-word', {
            active: playTime - word.timeLength >= word.startTime - numDetailedEarlySecs
          });
          detailedSentenceWords.push(<span className={wordClass} key={`w-${(i * 10) + j}`}>{word.value}</span>);
        } else if (word.value === '\r') {
          detailedSentenceWords.push(<span key={`wb-${(i * 10) + j}`}><br /></span>);
        } else if (word.value === '\n' || word.value === '\r\n') {
          detailedSentenceWords.push(<span key={`wb-${(i * 10) + j}`}><br /><br /></span>);
        }

      }

      detailedLyricsContent.push(
        <span className={detailedLyricClass} key={`detailed-lyric-${i}`}>{detailedSentenceWords}</span>
      );
    }

    if (detailedLyricsContent.length > 0) {
      fullLyricLines = detailedLyricsContent;
    }

    let voicesSupport = [];
    if (song.man_supported) voicesSupport.push(<span className="violet text" key="vm"><strong>♂</strong></span>);
    if (song.woman_supported) voicesSupport.push(<span className="pink text" key="vw"><strong>♀</strong></span>);

    let recommendedSongsContent;
    let recommendedSongItems = [];
    if (recommendedSongs.length > 0) {
      for (let i = 0; i < recommendedSongs.length; i++) {
        let recommendedSong = recommendedSongs[i];
        let recommendedSongItem =
              <a className="item" href={`/karaoke/${recommendedSong.slug}`} key={`r-${recommendedSong.slug}`}>
                {recommendedSong.name} ({recommendedSong.code})
              </a>;

        recommendedSongItems.push(recommendedSongItem);
      }

      let songLoadButtonClass = classNames('ui button mini primary basic', { loading: loadingSongs });

      recommendedSongsContent =
        <div className="ui center aligned recommended-songs purple segment">
          <h4>Các bài karaoke phổ biến khác:</h4>
          <div className="ui horizontal link list" style={{marginBottom: '10px'}}>
            {recommendedSongItems}
          </div>
          <a href="javascript:void(0)" className={songLoadButtonClass} onClick={this.onSongLoadClick}>
            Xem thêm các bài khác
          </a>
        </div>;
    }

    let likeBox =
          <div className="fb-like" data-href={songURL} data-layout="button_count" data-action="like" data-size="large"
               data-show-faces="true" data-share="true"></div>;

    let commentBox = <div className="fb-comments" data-href={songURL} data-width="100%"></div>;

    return (
      <div className="ui grid container songs-index">
        <div className="ui sixteen wide column">
          <div className="ui grid">
            <div className="eleven wide computer ten wide tablet sixteen wide mobile column">
              <div className="ui song-main segments">
                <div className="ui song-content center aligned main olive segment">
                  <h1 className="ui song-title header text center">
                    <div className="content text capitalize">
                      <i className="music icon" /> {song.name}
                      <div className="sub header">{song.code} {voicesSupport} {likeBox}</div>
                    </div>
                  </h1>

                  <div className="ui one statistics box">
                    <div className="blue statistic">
                      <div className="label">
                        {currentTime}
                      </div>
                    </div>
                  </div>

                  {actionsContent}

                  {lyricsContent}

                  {
                    false &&
                    <div className="ui icon message">
                      <i className="help circle outline icon" />
                      <div className="content">
                        <div className="header">
                          Bạn có biết?
                        </div>
                        <p>Chức năng thâu âm và hát với nhau đang được phát triển.</p>
                      </div>
                    </div>
                  }

                  <ins className="adsbygoogle"
                       style={{display: 'block'}}
                       data-ad-client="ca-pub-5534609998674740"
                       data-ad-slot="8233844527"
                       data-ad-format="auto">
                  </ins>
                </div>

                <div className="ui center aligned statistic-info segment">
                  <div className="ui one mini statistics">
                    <div className="statistic">
                      <div className="value">
                        <i className="microphone icon" /> {song.views_count}
                      </div>
                      <div className="label">
                        Lượt hát
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="five wide computer six wide tablet sixteen wide mobile column">
              {recommendedSongsContent}
            </div>
          </div>

          <div className="ui grid">
            <div className="eleven wide computer ten wide tablet sixteen wide mobile column">
              <div className="ui center aligned teal full-lyrics segment">
                <p>Lời bài hát</p>
                <h3 className="ui header">
                  {song.name}
                </h3>

                {fullLyricLines}
              </div>
            </div>

            <div className="five wide computer six wide tablet sixteen wide mobile column">
              <div className="ui blue comment segment">
                <h3 className="ui header">Tán gẫu về bài hát “{song.name}”</h3>
                {commentBox}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SongShow.propTypes = {
  song:    React.PropTypes.object,
  songURL: React.PropTypes.string
};
