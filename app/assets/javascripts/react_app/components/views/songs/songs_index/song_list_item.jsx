class SongListItem extends React.Component {
  render() {
    let { song } = this.props;

    let voicesSupport = [];
    if (song.man_supported) voicesSupport.push(<span className="violet text" key="vm"><strong>♂</strong></span>);
    if (song.woman_supported) voicesSupport.push(<span className="pink text" key="vw"><strong>♀</strong></span>);

    let lyricsPreview = '';
    if (song.lyrics && song.lyrics.length > 0) {
      lyricsPreview = `${song.lyrics.substring(0, 80)}...`;
    }

    return (
      <a className="item" href={`karaoke/${song.slug}`}>
        <i className="music icon" />
        <div className="content">
          <div className="header text capitalize">{song.name}</div>
          <div className="description">{song.code} {voicesSupport}</div>
          <div className="description k-lyrics-list-desc">{lyricsPreview}</div>
        </div>
      </a>
    );
  }
}

SongListItem.propTypes = {
  song: React.PropTypes.object
};
