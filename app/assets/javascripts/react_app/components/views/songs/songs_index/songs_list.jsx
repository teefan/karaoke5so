class SongsList extends React.Component {
  render() {
    let { children } = this.props;

    return (
      <div className="ui middle aligned divided big list">
        {children}
      </div>
    );
  }
}

SongsList.propTypes = {
  children: React.PropTypes.array
};
