class SongsIndex extends React.Component {
  constructor(props) {
    super(props);

    this.renderSongsList = this.renderSongsList.bind(this);
  }

  componentDidMount() {
    (adsbygoogle = window.adsbygoogle || []).push({});
  }

  renderSongsList(songs) {
    let songsContent = [];

    songsContent.push(
      <div className="item" key="ads-item">
        <ins className="adsbygoogle"
             style={{display: 'block'}}
             data-ad-client="ca-pub-5534609998674740"
             data-ad-slot="8233844527"
             data-ad-format="auto">
        </ins>
      </div>
    );

    for (let i = 0; i < songs.length; i++) {
      let songItem = <SongListItem song={songs[i]} key={`song-list-item-${songs[i].code}`} />;

      songsContent.push(songItem);
    }

    return (
      <SongsList>
        {songsContent}
      </SongsList>
    );
  }

  renderHeader(filter) {
    switch (filter) {
      case 'random':
        return '10 bài karaoke ngẫu nhiên';
      case 'man_and_woman':
        return '10 bài karaoke song ca hay';
      case 'man':
        return '10 bài karaoke giọng nam hay';
      case 'woman':
        return '10 bài karaoke giọng nữ hay';
      case 'most_viewed':
        return '10 bài karaoke nhiều người hát nhất';
      case 'featured':
      default:
        return '10 bài karaoke hay';
    }
  }

  buildHomeURL(filter) {
    switch (filter) {
      case 'random':
        return '/muoi_bai_karaoke_ngau_nhien';
      case 'man_and_woman':
        return '/muoi_bai_karaoke_song_ca';
      case 'man':
        return '/muoi_bai_karaoke_giong_nam';
      case 'woman':
        return '/muoi_bai_karaoke_giong_nu';
      case 'featured':
        return '/muoi_bai_karaoke_hay';
      case 'most_viewed':
        return '/muoi_bai_nhieu_nguoi_hat_nhat';
      default:
        return '/';
    }
  }

  render() {
    let { songs, filter } = this.props;

    return (
      <div className="ui stackable grid container songs-index">
        <div className="ui sixteen wide column">
          <h1 className="ui header center text">
            <div className="content">{this.renderHeader(filter)}</div>

            <p className="text center">
              <a href={this.buildHomeURL(filter)} className="ui small olive button">Tiếp 10 bài khác</a>
            </p>
          </h1>
        </div>

        <div className="ui eleven wide column">
          {this.renderSongsList(songs)}

          <p className="text center">
            <a href={this.buildHomeURL(filter)} className="ui small olive button">Tiếp 10 bài khác</a>
          </p>
        </div>

        <div className="ui five wide column text center">
          <FacebookPage />
        </div>
      </div>
    );
  }
}

SongsIndex.defaultProps = {
  songs: [],
  filter: ''
};

SongsIndex.propTypes = {
  songs: React.PropTypes.array,
  filter: React.PropTypes.string
};
