class SongsListShow extends React.Component {
  constructor(props) {
    super(props);

    this.renderSongsList = this.renderSongsList.bind(this);
  }

  componentDidMount() {
    (adsbygoogle = window.adsbygoogle || []).push({});
  }

  renderSongsList() {
    let { songs } = this.props;

    let songItems = [];
    for (let i = 0, len = songs.length; i < len; i++) {
      let song = songs[i];

      let voicesSupport = [];
      if (song.man_supported) voicesSupport.push(<span className="violet text" key="vm"><strong>♂</strong></span>);
      if (song.woman_supported) voicesSupport.push(<span className="pink text" key="vw"><strong>♀</strong></span>);

      let lyricsPreview = '';
      if (song.lyrics && song.lyrics.length > 0) {
        lyricsPreview = `${song.lyrics.substring(0, 120)}...`;
      }

      let songItem =
        <div className="ui five wide column songs-list-item" key={`song-list-item-${song.code}`}>
          <h4>
            <a href={`/karaoke/${song.slug}`} className="item text black">{song.name}</a>

            <div className="text small muted">
              {song.code} {voicesSupport} - <i className="microphone icon" style={{margin: 0}} />{song.views_count}
            </div>
            <div className="text small muted">{lyricsPreview}</div>
          </h4>
        </div>;

      songItems.push(songItem);
    }

    return songItems;
  }

  render() {
    let { songs, letter } = this.props;

    return (
      <div className="ui stackable three column grid container songs-list">
        <div className="ui sixteen wide column">
          <h3 className="text center">Danh sách các bài Karaoke: bắt đầu với chữ {letter.toUpperCase()}</h3>
          <ins className="adsbygoogle"
               style={{display: 'block'}}
               data-ad-client="ca-pub-5534609998674740"
               data-ad-slot="8233844527"
               data-ad-format="auto">
          </ins>
        </div>

        {this.renderSongsList()}
      </div>
    );
  }
}

SongsListShow.defaultProps = {
  songs: [],
  letter: ''
};

SongsListShow.propTypes = {
  songs: React.PropTypes.array,
  letter: React.PropTypes.string
};
