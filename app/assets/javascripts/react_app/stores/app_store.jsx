let $loadSongsXHR;

class AppStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      loadingSongs: false,
      recommendedSongs: []
    };
  }

  loadRecommendedSongs(state, action) {
    let newState = Object.assign({}, state);

    if ($loadSongsXHR) $loadSongsXHR.abort();

    newState.loadingSongs = true;

    $loadSongsXHR = $.ajax({
      url: '/karaoke.json',
      method: 'GET',
      dataType: 'json'
    });

    $loadSongsXHR.done((response, status, xhr) => {
      AppDispatcher.dispatch({
        type: 'LOAD_RECOMMENDED_SONGS_DONE',
        response
      });
    }).fail((xhr, status, error) => {

    });

    return newState;
  }

  loadRecommendedSongsDone(state, action) {
    let newState = Object.assign({}, state);

    newState.loadingSongs = false;
    newState.recommendedSongs = action.response;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'LOAD_RECOMMENDED_SONGS':
        return this.loadRecommendedSongs(state, action);

      case 'LOAD_RECOMMENDED_SONGS_DONE':
        return this.loadRecommendedSongsDone(state, action);

      default:
        return state;
    }
  }
}

const AppStore = new AppStoreClass();
