let $fileRequestXHR;

class SongStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      fileData: null,
      requestingFile: false
    };
  }

  loadFile(state, action) {
    let newState = Object.assign({}, state);

    if ($fileRequestXHR) $fileRequestXHR.abort();

    newState.requestingFile = true;

    $fileRequestXHR = $.ajax({
      url: `/data/${action.songCode}`,
      method: 'GET'
    });

    $fileRequestXHR.done((response, status, xhr) => {
      AppDispatcher.dispatch({
        type: 'LOAD_FILE_DONE',
        response
      });
    }).fail((xhr, status, error) => {

    });

    return newState;
  }

  loadFileDone(state, action) {
    let newState = Object.assign({}, state);

    newState.fileData = action.response;
    newState.requestingFile = false;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'LOAD_FILE':
        return this.loadFile(state, action);

      case 'LOAD_FILE_DONE':
        return this.loadFileDone(state, action);

      default:
        return state;
    }
  }
}

const SongStore = new SongStoreClass();
