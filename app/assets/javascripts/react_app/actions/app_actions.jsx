const AppActions = {
  loadRecommendedSongs() {
    AppDispatcher.dispatch({
      type: 'LOAD_RECOMMENDED_SONGS'
    });
  },
};
