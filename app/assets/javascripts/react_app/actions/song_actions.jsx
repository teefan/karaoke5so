const SongActions = {
  loadFile(songCode) {
    AppDispatcher.dispatch({
      type: 'LOAD_FILE',
      songCode
    });
  },
};
