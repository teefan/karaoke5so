# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  default from: 'thongbao@karaoke5so.com'
  layout 'mailer'
end
