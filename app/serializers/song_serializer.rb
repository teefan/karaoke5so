# frozen_string_literal: true
class SongSerializer < ActiveModel::Serializer
  attributes :id, :name, :name_en, :name_s, :code, :lyrics_vni, :lyrics, :views_count, :plays_count, :slug
end
