# frozen_string_literal: true
#
# == Schema Information
#
# Table name: songs
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  name_en         :string(255)
#  name_s          :string(255)
#  code            :string(255)      not null
#  lyrics_vni      :text(65535)
#  lyrics          :text(65535)
#  views_count     :integer          default("0")
#  plays_count     :integer          default("0")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  man_supported   :boolean
#  woman_supported :boolean
#  slug            :string(255)
#  featured        :boolean          default("0")
#
# Indexes
#
#  index_songs_on_code                               (code)
#  index_songs_on_featured                           (featured)
#  index_songs_on_man_supported                      (man_supported)
#  index_songs_on_man_supported_and_woman_supported  (man_supported,woman_supported)
#  index_songs_on_name                               (name)
#  index_songs_on_name_en                            (name_en)
#  index_songs_on_name_s                             (name_s)
#  index_songs_on_plays_count                        (plays_count)
#  index_songs_on_slug                               (slug) UNIQUE
#  index_songs_on_views_count                        (views_count)
#  index_songs_on_woman_supported                    (woman_supported)
#

class Song < ApplicationRecord
  validates :code, presence: true

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  is_impressionable counter_cache: true, column_name: :views_count, unique: :all

  scope :featured, -> { where(featured: true) }

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      [:name, :code],
      [:name, :code, :id]
    ]
  end
end
