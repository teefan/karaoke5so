# frozen_string_literal: true
module ApplicationHelper
  def app_title
    return "#{song_name_titleize} - mã số #{@song['code']} | Karaoke beat chuẩn" if @song

    build_app_title(params[:filter], 'Hát Karaoke 5 số Online')
  end

  def build_app_title(filter, common_header) # rubocop:disable CyclomaticComplexity
    case filter
    when 'random'
      "10 bài karaoke ngẫu nhiên - #{common_header}"
    when 'featured'
      "10 bài karaoke hay - #{common_header}"
    when 'most_viewed'
      "10 bài karaoke nhiều người hát nhất - #{common_header}"
    when 'man_and_woman'
      "10 bài karaoke song ca hay - #{common_header}"
    when 'man'
      "10 bài karaoke giọng nam hay - #{common_header}"
    when 'woman'
      "10 bài karaoke giọng nữ hay - #{common_header}"
    else
      common_header
    end
  end

  def app_description
    desc = 'Hát karaoke trực tuyến miễn phí hơn 16.000 bài karaoke kỹ thuật số chất lượng cao ' \
           'với lời và âm thanh lossless 16-bit 44.1KHz.'

    if @song && @song['lyrics'].present?
      "Lời bài hát #{song_name_titleize}: #{@song['lyrics'].gsub(/[\r\n]/, '').truncate(180, separator: /\s/)}"
    elsif @song
      "Bài hát #{song_name_titleize} - #{desc}"
    else
      desc
    end
  end

  def app_current_url
    url_for(only_path: false)
  end

  def og_type
    current_type = 'website'
    current_type = 'music.song' if @song
    current_type
  end

  def app_keywords
    return 'hát karaoke 5 số trên máy tính trực tuyến miễn phí' unless @song

    <<-HTML
    <p>#{song_name_downcase} beat chuẩn, #{@song['name_en'].downcase} karaoke trực tuyến miễn phí</p>
    HTML
  end

  def song_name_titleize
    @song['name'].mb_chars.titleize
  end

  def song_name_upcase
    @song['name'].mb_chars.upcase
  end

  def song_name_downcase
    @song['name'].mb_chars.downcase
  end
end
