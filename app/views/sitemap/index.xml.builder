xml = Builder::XmlMarkup.new
xml.instruct! :xml, version: '1.0'

xml.urlset(xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9') do
  @songs.find_each(batch_size: 100) do |song|
    xml.url do
      xml.loc song_url(song)
    end
  end
end
