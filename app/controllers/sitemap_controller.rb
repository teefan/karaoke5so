# frozen_string_literal: true
class SitemapController < ApplicationController
  respond_to :xml

  def index
    @songs = Song.select(:id, :slug).order(id: 'DESC').all

    respond_with @songs
  end
end
