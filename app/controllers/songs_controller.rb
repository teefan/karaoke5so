# frozen_string_literal: true
class SongsController < ApplicationController
  respond_to :html, :json

  def index
    @songs = Song.ransack(process_search_query).result(distinct: true)

    @songs = filter_songs(@songs)
    @songs = @songs.page(params[:page]) unless params[:q] == 'all_featured'

    respond_with(@songs)
  end

  def show
    @song = Song.friendly.find(params[:id])

    impressionist(@song, nil, controller_name: params[:controller_name], action_name: params[:action_name])
    @song.views_count += 1 # simulate impressionist count added

    respond_with(@song)
  end

  private

  def process_search_query
    return nil if params[:q].blank? || %w(random most_viewed all_featured).include?(params[:q])

    params[:q].each_key do |k|
      query = params[:q][k]

      params[:q][k] = query.strip if query && query.is_a?(String)
    end

    params[:q]
  end

  def filter_songs(songs) # rubocop:disable AbcSize, MethodLength
    # min_views_count = 20 # now that we're already having views_count! yay
    max_top_songs = 100 # get first top songs to random

    if params[:q] == 'most_viewed'
      song_ids = Impression.where(impressionable_type: 'Song')
                           .where('created_at >= ? AND created_at <= ?',
                                  DateTime.now.in_time_zone - 30.days,
                                  DateTime.now.in_time_zone)
                           .pluck(:impressionable_id)
      top_song_ids = Song.where(id: song_ids.uniq)
                         .order('VIEWS_COUNT DESC')
                         .limit(max_top_songs).ids
      return Song.where(id: top_song_ids).order('RAND()')
    end

    if params[:q].blank? ||
       params[:q] == 'random'
      # return songs.where("CAST(code AS SIGNED) > 49999 AND VIEWS_COUNT >= #{min_views_count}").order('RAND()')
      top_song_ids = Song.where('CAST(code AS SIGNED) > 49999')
                         .order('VIEWS_COUNT DESC')
                         .limit(max_top_songs).ids
      return Song.where(id: top_song_ids).order('RAND()')
    end

    return songs.featured.order(code: :desc) if params[:q] == 'all_featured'

    params[:q].each do |_k, v|
      return songs if v.blank?
    end

    songs.order('RAND()')
  end
end
