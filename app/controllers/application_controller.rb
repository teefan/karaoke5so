require 'application_responder'

# frozen_string_literal: true
class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :null_session # :exception

  before_action :group_letters

  def group_letters
    # @letters = Song.group('UPPER(LEFT(name_en, 1))').where('CAST(code AS SIGNED) > 49999').count
    # cache letters
    @letters = {
      '1' => 13, '2' => 2, '5' => 1, '6' => 1, '8' => 1, '9' => 3, 'A' => 372, 'B' => 662, 'C' => 1424, 'D' => 1042,
      'E' => 273, 'F' => 3, 'G' => 451, 'H' => 703, 'I' => 9, 'K' => 438, 'L' => 467, 'M' => 808, 'N' => 1341,
      'O' => 38, 'P' => 149, 'Q' => 130, 'R' => 123, 'S' => 258, 'T' => 1703, 'U' => 42, 'V' => 450, 'W' => 3,
      'X' => 221, 'Y' => 182
    }
  end
end
