require 'rc4'

# frozen_string_literal: true
class DataFilesController < ApplicationController
  # GET /data_files/1
  def show
    song = Song.find_by(code: params[:id])
    unless song
      render json: { data: nil }
      return
    end

    file_data = File.read(file_path(song))

    encrypted_file_data = data_encrypt(file_data, song.code, song.name_s)

    render json: { data: encrypted_file_data }
  end

  private

  def file_path(song)
    Rails.root.join('lib', 'midi', 'extracted', detect_file_language(song), build_file_name(song))
  end

  def build_file_name(song)
    "#{song.name_en} #{song.code}".parameterize + '.mid'
  end

  def detect_file_language(song)
    song.code.to_i >= 30_000 && song.code.to_i < 50_000 ? 'eg' : 'vn'
  end

  def data_encrypt(data, song_code, song_name_search)
    random_key = SecureRandom.hex(50)

    base64_data = Base64.encode64(data)

    encryptor = RC4.new("#{song_code}-#{song_name_search}-#{random_key}")

    encrypted_data = encryptor.encrypt(base64_data)

    "#{Base64.encode64(encrypted_data)}:#{random_key}"
  end
end
