# frozen_string_literal: true
class SongsListController < ApplicationController
  respond_to :html

  def show
    @songs = Song.where('CAST(code AS SIGNED) > 49999 AND LOWER(LEFT(name_en, 1)) = ?', params[:id])
                 .order(name: :asc)

    respond_with(@songs)
  end
end
